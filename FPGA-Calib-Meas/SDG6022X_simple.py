#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Author: Erik-E
# Date: 2023-01-25
 
import pyvisa
import time

class SDG6022X():
    """ Class to control the Siglent SDG6022X Arbitrary Waveform Generator
    pyvisa with the respective drivers need to be installed
    Unfortunately that driver part is rather complicated.
    Test the driver install with the `pyvias-info` script.

    When using Linux following packages are required:
        pyvisa, pyvisa-py, pyserial, pyusb, zeroconf, gpib-ctypes (optional)

    When using Windows:
    * try to install the NI-VISA package using the NI-PacketManager
    * I am not sure if the siglent ivi driver is required.
    """
    
    '''
    Version without write_binary_values since this seems to be the cause for timeout errors.
    
    '''
    # Programming info for the generator is at
    # https://siglentna.com/USA_website_2014/Documents/Program_Material/SDG_ProgrammingGuide_PG_E03B.pdf

    def __init__(self, debug=False):
        if debug:
            pyvisa.log_to_screen()

        # open the visa-lib and try to find the device
        # Added '@py' to fix the problem of not finding device when ni-visa is installed
        
        self.rm = pyvisa.ResourceManager('@py')
        dev = list(filter(lambda name: "SDG" in name, list(self.rm.list_resources())))
        if (len(dev)) == 0:
            raise IOError("No SDG device found")
        self.dev = self.rm.open_resource(dev[0], timeout=50000, chunk_size = 24*1024*1024)

        # set some defaults for the communication        
        self.dev.query_delay = 0.1
        self.dev.write_termination = ''
        
    def output(self, channel, on_off, load=50):
        mode = ("ON" if on_off else "OFF")
        lo = ("HZ" if load == 0 else load)
        self.dev.write(f"C{channel}:OUTP LOAD,{lo}")
        self.dev.write(f"C{channel}:OUTP {mode}")

    def reset(self):
        self.dev.write('*RST')

    def configure_waveform_ramp(self, channel, freq, amp, delay, offset = 0, phase = 0, symety = 50):
        '''
        Sets the Waveform of selected channel to ramp and configures its parameters.

        Parameters
        ----------
        channel : int
            Channel which will be changed.
        freq : int
            Frequency in [Hz].
        amp : float
            Amplitude in [V].
        offset : float, optional
            Constant offset of waveform in [V]. The default is 0.
        phase : float, optional
            Phase offset in [°]. The default is 0.
        symety : float, optional
            Fraction between rising an falling flank in [%]. The default is 50.

        Raises
        ------
        IOError
            When channel is neither 1 or 2.
            When freq is larger than 200MHz
            
        Returns
        -------
        None.

        '''
        if channel not in [1, 2]:
            raise IOError("Channel needs to be 1 or 2")
        if freq >= 200000000:
            raise IOError(f"{freq}Hz is greater than the max frequency of 200MHz")

        self.dev.write('C%d:BaSic_WaVe WVTP, RAMP'%(channel))
        self.dev.write('C%d:BaSic_WaVe FRQ, %f'%(channel, freq))
        self.dev.write('C%d:BaSic_WaVe HLEV, %f'%(channel, amp))
        self.dev.write('C%d:BaSic_WaVe LLEV, %f'%(channel, offset))
        self.dev.write('C%d:BaSic_WaVe PHSE, %f'%(channel, phase))
        self.dev.write('C%d:BaSic_WaVe SYM, %f'%(channel, symety))
        self.dev.write('C%d:BaSic_WaVe DLY, %f'%(channel, delay))


    def configure_waveform_pulse(self, channel, freq, amp, pulse_width, rising_edge = 2e-9, offset = 0, delay = 0):
        '''
        Sets the Waveform of selected channel to pulse and configures its parameters.

        Parameters
        ----------
        channel : int
            Channel which will be changed.
        freq : int
            Frequency in [Hz].
        amp : float
            Amplitude in [V].
        offset : float, optional
            Constant offset of waveform in [V]. The default is 0.
        pulse_width : float
            Width of pulse in [s].
        rising_edge : float, optional
            Time for rising edge in [s]. The default is 2e-9.
        offset : float, optional
            Constant offset of waveform in [V]. The default is 0.
        delay : TYPE, optional
            Time between start of cycle and start of pulse in [s]. The default is 0.

        Raises
        ------
        IOError
            When channel is neither 1 or 2.
            When freq is larger than 200MHz
            
        Returns
        -------
        None.

        '''
        if channel not in [1, 2]:
            raise IOError("Channel needs to be 1 or 2")
        if freq >= 200000000:
            raise IOError(f"{freq}Hz is greater than the max frequency of 200MHz")

        self.dev.write('C%d:BaSic_WaVe WVTP, PULSE'%(channel))
        self.dev.write('C%d:BaSic_WaVe FRQ, %f'%(channel, freq))
        self.dev.write('C%d:BaSic_WaVe HLEV, %f'%(channel, amp))
        self.dev.write('C%d:BaSic_WaVe WIDTH, %.8f'%(channel, pulse_width))
        self.dev.write('C%d:BaSic_WaVe RISE, %f'%(channel, rising_edge))
        self.dev.write('C%d:BaSic_WaVe LLEV, %f'%(channel, offset))
        self.dev.write('C%d:BaSic_WaVe DLY, %.8f'%(channel, delay))

 
    def configure_burst(self, channel, on_off, period, start_phase = 0):
        '''
        Configutes burst mode for selected channel. 
        One period of the waveform of the channel will be generated the selected number of seconds apart.
        
        Not all parameters of Burst are implemented.
        
        Parameters
        ----------
        channel : int
            Channel which will be changed.
        on_off : bool
            If the burst ist to be turned on or off.
        period : floaz
            Feriod at which the burst ist repeted in [s].
        start_phase : float, optional.
            Phase offset for the start of the pulse in [°]. The default is 0.

        Raises
        ------
        IOError
            When channel is neither 1 or 2.

        Returns
        -------
        None.

        '''
        if channel not in [1, 2]:
            raise IOError("Channel needs to be 1 or 2")
    
        mode = ("ON" if on_off else "OFF")
        
        self.dev.write('C%d:BursTWaVe STATE, '%(channel) + mode)
        self.dev.write('C%d:BursTWaVe PRD, %f'%(channel, period))
        self.dev.write('C%d:BursTWaVe STPS, %f'%(channel, start_phase))
        
    def mvt_voltage_setup(self, V_ramp_max = 0.5, V_ramp_min = 0.1, V_pulse = 1.5, rising_edge = 1e-6, sync_pulse_length = 1e-7, pulse_period = 1e-3):
        '''
        Sets the channel 1 and 2 to desired modes for mvt calibration.

        Parameters
        ----------
        V_ramp_max : float, optional
            Peak voltage of the triangle signal in [V]. The default is 0.9.
        V_ramp_min : float, optional
            Base voltage of the triangle signal in [V]. The default is 0.7.
        V_pulse : float, optional
            Voltage of the trigger peak. The default is 1.5.
        rising_edge : float, optional
            Time from start of triangle to peak voltage in [s]. The default is 1e-6.
        sync_pulse_length : float, optinoal
            Time
        pulse_period : float, optional
            Time between two pulses in [s]. The default is 1e-3.

        Returns
        -------
        None.

        '''
        self.output(1, False)
        self.output(2, False)
        self.reset()
        self.configure_waveform_ramp(1, 1/(2*rising_edge), V_ramp_max*4, sync_pulse_length, offset = V_ramp_min*4 ) 
        self.configure_burst(1, True, pulse_period, -90)
        self.configure_waveform_pulse(2, 1/(2*sync_pulse_length), V_pulse*4, sync_pulse_length, offset =  0)
        self.configure_burst(2, True, pulse_period)
        self.output(1, True)
        self.output(2, True)
 
    def mvt_time_setup(self):
        self.output(1, False)
        self.output(2, False)
        self.reset()
        self.configure_waveform_pulse(1, 1e3, 0.750*4, 50e-9, rising_edge = 2e-9, offset = 0, delay = 550e-9)
        self.configure_waveform_pulse(2, 1e3, 1*4, 50e-9, rising_edge = 2e-9, offset = 0, delay = 0)
        self.output(1, True)
        self.output(2, True)
        
if __name__ == '__main__':
    generator = SDG6022X()
    #generator.mvt_voltage_setup()
    generator.mvt_time_setup()
    time.sleep(2)
    generator.rm.close()
