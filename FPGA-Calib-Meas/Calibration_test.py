
import serial.tools.list_ports
import serial
import time
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm
from numba import njit
import SDG6022X_simple

PARITY1 = lambda i: i == 1
PARITY2 = lambda i: PARITY1(i & 0x01) ^ PARITY1(i >> 1)
PARITY4 = lambda i: PARITY2(i & 0x03) ^ PARITY2(i >> 2)
PARITY8 = lambda i: PARITY4(i & 0x0f) ^ PARITY4(i >> 4)

parity_look_up = np.array([PARITY8(np.uint8(i)) for i in range(256)], dtype = bool)

@njit
def encode(Timestamp, Channel_number, MVT_Channel = 0, Is_rising = True):
    write_buffer = np.zeros(10, dtype = np.uint8);
    write_buffer[0] =  (Is_rising <<  np.uint8(6)) |  (MVT_Channel <<  np.uint8(4)) |  (Channel_number <<  np.uint8(1) ) |  (Timestamp >>  np.uint8(63))
    write_buffer[1] =  np.uint8(1 << 7) |  ( (Timestamp >> np.uint8(56)) & np.uint8(0x7F))
    write_buffer[2] =  np.uint8(1 << 7) |  ( (Timestamp >> np.uint8(49)) & np.uint8(0x7F))
    write_buffer[3] =  np.uint8(1 << 7) |  ( (Timestamp >> np.uint8(42)) & np.uint8(0x7F))
    write_buffer[4] =  np.uint8(1 << 7) |  ( (Timestamp >> np.uint8(35)) & np.uint8(0x7F))
    write_buffer[5] =  ( (Timestamp >> np.uint8(28)) & np.uint8(0x7F))
    write_buffer[6] =  ( (Timestamp >> np.uint8(21)) & np.uint8(0x7F))
    write_buffer[7] =  ( (Timestamp >> np.uint8(13)) & np.uint8(0xFF))
    write_buffer[8] =  ( (Timestamp >> np.uint8( 5)) & np.uint8(0xFF))
    write_buffer[9] =  ( (Timestamp << np.uint8( 3)) & np.uint8(0xFF))
    
    p0 = (write_buffer[7] >> 7)  ^ (write_buffer[8] >> 7) ^ (write_buffer[9] >> 7)
    p1 = parity_look_up[write_buffer[0]] ^ parity_look_up[write_buffer[1]] ^ parity_look_up[write_buffer[2]] ^ parity_look_up[write_buffer[3]] ^ parity_look_up[write_buffer[4]]
    p2 = parity_look_up[write_buffer[5]] ^ parity_look_up[write_buffer[6]] ^ parity_look_up[write_buffer[7]] ^ parity_look_up[write_buffer[8]] ^ parity_look_up[write_buffer[9]]
    write_buffer[9] = write_buffer[9] | (p0 << 2) | (p1 << 1) | (p2)

    #print('{:08b}\n{:08b}\n{:08b}\n{:08b}\n{:08b}\n{:08b}\n{:08b}\n{:08b}\n{:08b}\n{:08b}'.format(*write_buffer))
    return(write_buffer)

@njit
def decode(data_in, check_parity = True):
    parity = True
    if (check_parity == True):
        p0 = ( data_in[7] >> 7)  ^ ( data_in[8] >> 7) ^ ( data_in[9] >> 7)
        p1 = parity_look_up[ data_in[0]] ^ parity_look_up[ data_in[1]] ^ parity_look_up[ data_in[2]] ^ parity_look_up[ data_in[3]] ^ parity_look_up[ data_in[4]]
        p2 = parity_look_up[ data_in[5]] ^ parity_look_up[ data_in[6]] ^ parity_look_up[ data_in[7]] ^ parity_look_up[ data_in[8]] ^ (parity_look_up[ data_in[9] & 0xf8])
        parity = (np.uint8(p0)*4+np.uint8(p1)*2+np.uint8(p2)) == (data_in[9] & 0x07)
    
    is_rising =  (data_in[0] & 0x40) == 0x40
    Mvt_channel = (data_in[0] & 0x30) >> 4
    Channel = (data_in[0] & 0x0e) >> 1
    timestamp  = (np.uint64(data_in[0] & 0x01) << np.uint8(63))
    timestamp += (np.uint64(data_in[1] & 0x7F) << np.uint8(56))
    timestamp += (np.uint64(data_in[2] & 0x7F) << np.uint8(49))
    timestamp += (np.uint64(data_in[3] & 0x7F) << np.uint8(42))
    timestamp += (np.uint64(data_in[4] & 0x7F) << np.uint8(35))
    timestamp += (np.uint64(data_in[5] & 0x7F) << np.uint8(28))
    timestamp += (np.uint64(data_in[6] & 0x7F) << np.uint8(21))
    timestamp += (np.uint64(data_in[7] & 0xFF) << np.uint8(13))
    timestamp += (np.uint64(data_in[8] & 0xFF) << np.uint8( 5))
    timestamp += (np.uint64(data_in[9] & 0xF8) >> np.uint8( 3))
    return(timestamp, is_rising, Mvt_channel, Channel, parity)    
    
def time_to_voltage(time_in, V_ramp_max = 1, V_ramp_min = 0.2, rising_edge = 1e-6, sync_pulse_length = 1e-7):
    time = time_in + sync_pulse_length
    delta_V = V_ramp_max- V_ramp_min
    dVdt = delta_V/rising_edge
    return time*dVdt + V_ramp_min

def Set_trigger_voltage(voltage, channel, MVT_Channel, ser):
    DAC_bits = int(voltage*(2**10-1)/2.5)
    ser.write(bytes((0x00, (channel << 4) + MVT_Channel , DAC_bits >> 8, DAC_bits & 0xff)))

def reset_all_fifos(ser):
    ser.write(bytes((0x01,)))

def reset_DeSer_fifos(ser):
    ser.write(bytes((0x02,)))


def read_data(ser, n = 1000):
    ser.read_all()
    reset_all_fifos(ser)
    
    position = 0
    sequence = np.array([0, 1, 1, 1, 1, 0, 0], dtype = np.uint8)
    read = np.zeros(10, dtype = np.uint8)    
    with open('./debug.txt', 'w') as debug:
        with tqdm(total=n) as pbar:
            with open('./timestamps.txt', 'w') as f:
                f.write('# timestamp of 800MHz clock, MVTChannel, Channel')
                i = 0
                ser.read_all()
                while(i < n):
                    a = ser.read(1)[0]
                    if((position == 0) and ((a >> 7) == 1)):
                        debug.write(str(a) + '\n')
                    if position < 7:
                        if (a >> 7) == sequence[position]:
                            read[position] = np.uint8(a)
                            position += 1
                        elif( (a >> 7) == 0):
                            read[0] = np.uint8(a)
                            position = 1
                        else:
                            position = 0 
                    elif(position == 9):
                        read[position] = np.uint8(a)
                        payload = decode(read, True)
                        if(payload[-1]): f.write('%d, %d, %d\n'%(payload[0], payload[2], payload[3]))
                        position = 0
                        pbar.update(1)
                        i += 1
                    else:
                        read[position] = np.uint8(a)
                        position += 1
    
def preprocess_data(expected_mean, expected_width, txt = './timestamps.txt'):
    data = np.loadtxt(txt, dtype= np.uint64, delimiter= ',', usecols = 0)
    data = data/8e8
    
    mask = np.greater(abs(data - expected_mean), expected_width*5)
    data_err = data[mask]
    data = data[~mask]
    
    mask = np.greater(abs(data - data.mean()), 5*data.std())
    data_err = np.concatenate((data_err,data[mask]))
    data = data[~mask]
    
    while(mask.sum() > 0):
        mask = np.greater(abs(data - data.mean()), 5*data.std())
        data_err = np.concatenate((data_err,data[mask]))
        data = data[~mask]
        
    return data, data_err
    
def timing_plot(ser, data, ax):  
    data = data*1e9
    step = 1/8e8*1e9
    text = '(%.1f +/- %.1f) ns'%(data.mean(), data.std())
    ax.hist(data, bins = np.arange(data.min()-2*step, data.max()+2*step, step), rwidth = 0.9, label = text)
    ax.legend()
    return(data.mean(), data.std())

def voltage_plot(ser, delta_t, data, ax, step = 0.001):
    Voltage = time_to_voltage(data)
    text = '(%.1f +/- %.1f) mV'%(Voltage.mean()*1000, Voltage.std()*1000)
    ax.hist(Voltage, bins = np.arange(Voltage.min()-2*step, Voltage.max()+2*step, step), rwidth = 0.9, label = text)
    ax.legend()
    return(Voltage.mean(), Voltage.std())

def calibration_run(board, trigger_voltage = 0.6, MVT_Channel = 0, N = 5000):
    # creas calibation table
    calibration_data = np.zeros((8,5))
    
    # Calculate expected time delay of voltage calibration
    sync_pulse_length = 1e-7
    V_ramp_max = 1
    V_ramp_min = 0.2
    rising_edge = 1e-6
    dtdV = rising_edge/(V_ramp_max - V_ramp_min)
    expected_mean = dtdV*(trigger_voltage - V_ramp_min) - sync_pulse_length
    expected_width = expected_mean/20
    
    # Generate plots
    fig, ax = plt.subplots(8,3, sharey=True, sharex='col', figsize = (20,10))
    fig.suptitle('Calibration measurements MVT_Channel %d'%(MVT_Channel), fontsize = 16)
    ax[0][0].set_title('Timing offset')
    ax[0][1].set_title('Voltage meas. 1')
    ax[0][2].set_title('Voltage meas. 2')
    for i in range(8): ax[i,0].set_ylabel('Ch. %d'%i)
    for i in range(1,3): ax[7,i].set_xlabel('V')
    ax[7,0].set_xlabel('ns')
    
    # Connect serial and to function generator
    adress = serial.tools.list_ports.comports()[0].device
    ser = serial.Serial(adress, 921600)#921600
    generator = SDG6022X_simple.SDG6022X()
    
    
    # Set starting trigger voltage 
    for channel in range(8):
        input('Calibration Ch. %d: Switch to channel %d'%(channel, channel))
    
        Set_trigger_voltage(trigger_voltage, channel, MVT_Channel, ser)
        print('Calibration Ch. %d: Voltage set to %.3f'%(channel, trigger_voltage))
        
        # Set up two delayed pulses (deta t = 500ns)
        generator.mvt_time_setup()
        time.sleep(0.5)
        
        # reset FPGA 
        reset_all_fifos(ser)
        reset_DeSer_fifos(ser)
        # measure 2000 hits
        read_data(ser, N)
        #Plot timing hits
        data ,_ = preprocess_data(540e-9, 30e-9)
        t_mean, t_std = timing_plot(ser,data, ax[channel][0])
        time_offset = abs(500e-9-t_mean)
        print('Calibration Ch. %d: Timing offset  %.3f'%(channel, time_offset))

        # Set up triangle pulse
        generator.mvt_voltage_setup()
        time.sleep(0.5)
        ser.read_all()
        
        # reset FPGA 
        reset_all_fifos(ser)
        reset_DeSer_fifos(ser)
        # measure 2000 hits
        read_data(ser, N)
        #Plot voltages
        data ,_ = preprocess_data(expected_mean, expected_width)
        V_mean, V_std = voltage_plot(ser, time_offset, data, ax[channel, 1], 1/8e8/dtdV)
        print('Calibration Ch. %d: meas. voltage  %.3f +- %.3f'%(channel, V_mean, V_std))
        
        if(V_mean < 0.9 and V_mean > 0.3):
            Set_trigger_voltage(trigger_voltage + (trigger_voltage-V_mean), channel, MVT_Channel, ser)
            Set_voltage = trigger_voltage + (trigger_voltage-V_mean)
            print('Calibration Ch. %d: Voltage set to %.3f'%(channel, trigger_voltage + (trigger_voltage-V_mean)))
        else: 
            print('-----------!!!!! EROR !!!!!---------')
            print('Measure voltage not 0.3V < V_meas < 0.9V')
            break
        
        # reset FPGA 
        reset_all_fifos(ser)
        reset_DeSer_fifos(ser)
        ser.read_all()
        # measure 2000 hits
        read_data(ser, N)
        #Plot voltages
        data, data_err = preprocess_data(expected_mean, expected_width)
        V_mean, V_std = voltage_plot(ser, time_offset, data, ax[channel, 2])
        print('Calibration Ch. %d: meas. voltage  %.3f +- %.3f'%(channel, V_mean, V_std))
        calibration_data[channel] = np.array([Set_voltage, V_mean, V_std, len(data_err), t_mean-500])
 
    header = 'Calibrated voltage, measured voltage mean, measured voltage std, num of error timestamps, timing offset'
    np.savetxt('./calibration_MVT_%02d_%d_at_%d_mv.txt'%(board, MVT_Channel, trigger_voltage* 1000), calibration_data,  header = header)
    np.savetxt('./FPGA_Calibration_MVT0_T20_%02d.txt'%(board), calibration_data, header = header)
    fig.savefig('./calibration_MVT_%02d_%d_at_%d_mv.pdf'%(board, MVT_Channel, trigger_voltage* 1000))
    generator.rm.close()


if __name__ == '__main__':
    calibration_run(board = 11, N = 5000) # Board [00, 01, 10, 11]




