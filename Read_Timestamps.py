import sys
sys.path.insert(0,'./I2C_Control/')

import RPi.GPIO as GPIO
import serial.tools.list_ports
import serial
import time
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm
from numba import njit
from I2C_control import set_V_bias
import Si5338A_config
import warnings

# Functions needed for the parity check
PARITY1 = lambda i: i == 1
PARITY2 = lambda i: PARITY1(i & 0x01) ^ PARITY1(i >> 1)
PARITY4 = lambda i: PARITY2(i & 0x03) ^ PARITY2(i >> 2)
PARITY8 = lambda i: PARITY4(i & 0x0f) ^ PARITY4(i >> 4)
parity_look_up = np.array([PARITY8(np.uint8(i)) for i in range(256)], dtype = bool)

@njit
def decode(data_in, check_parity = True):
    '''
    Decode a backages of 10 bytes back into a timestamp and metadata.

    Parameters
    ----------
    data_in : array of length 10
        10 encoded data bytes.
    check_parity : bool, optional
        Flag to calculate the parity check. The default is True.

    Returns
    -------
    None.

    '''
    parity = True
    if (check_parity == True):
        p0 = ( data_in[7] >> 7)  ^ ( data_in[8] >> 7) ^ ( data_in[9] >> 7)
        p1 = parity_look_up[ data_in[0]] ^ parity_look_up[ data_in[1]] ^ parity_look_up[ data_in[2]] ^ parity_look_up[ data_in[3]] ^ parity_look_up[ data_in[4]]
        p2 = parity_look_up[ data_in[5]] ^ parity_look_up[ data_in[6]] ^ parity_look_up[ data_in[7]] ^ parity_look_up[ data_in[8]] ^ (parity_look_up[ data_in[9] & 0xf8])
        parity = (np.uint8(p0)*4+np.uint8(p1)*2+np.uint8(p2)) == (data_in[9] & 0x07)
    
    is_rising =  (data_in[0] & 0x40) == 0x40
    Mvt_channel = (data_in[0] & 0x30) >> 4
    Channel = (data_in[0] & 0x0e) >> 1
    timestamp  = (np.uint64(data_in[0] & 0x01) << np.uint8(63))
    timestamp += (np.uint64(data_in[1] & 0x7F) << np.uint8(56))
    timestamp += (np.uint64(data_in[2] & 0x7F) << np.uint8(49))
    timestamp += (np.uint64(data_in[3] & 0x7F) << np.uint8(42))
    timestamp += (np.uint64(data_in[4] & 0x7F) << np.uint8(35))
    timestamp += (np.uint64(data_in[5] & 0x7F) << np.uint8(28))
    timestamp += (np.uint64(data_in[6] & 0x7F) << np.uint8(21))
    timestamp += (np.uint64(data_in[7] & 0xFF) << np.uint8(13))
    timestamp += (np.uint64(data_in[8] & 0xFF) << np.uint8( 5))
    timestamp += (np.uint64(data_in[9] & 0xF8) >> np.uint8( 3))
    return(timestamp, is_rising, Mvt_channel, Channel, parity)    
    
def reset_all_FIFOs(ser):
    '''
    Reset the FIFOs of the FPGAs.

    Parameters
    ----------
    ser : Serial port object
        Opend Serial port object.

    Returns
    -------
    None.

    '''
    ser.write(bytes((0x01,)))

def reset_DeSer_FIFOs(ser):
    '''
    Reset the deserialiser FIFOs of the FPGAs.

    Parameters
    ----------
    ser : Serial port object
        Opend Serial port object.

    Returns
    -------
    None.

    '''
    ser.write(bytes((0x02,)))

def set_trigger_voltage_one_channel(voltage, channel, MVT_Channel, ser):
    '''
    Set the trigger voltage of one channle via serial.

    Parameters
    ----------
    voltage : float
        trigger voltage [0.4V, 1.3V].
    channel : int
        Mezz Channle of the board [0, 7].
    MVT_Channel : int
        MVT_Channel of the Mezz Channel [0, 3]. Currently oly MVT_Channel 0 is read out
    ser :Serial port object
        Opend Serial port object.

    Returns
    -------
    None.

    '''
    DAC_bits = int(voltage*(2**10-1)/2.5)
    ser.write(bytes((0x00, (channel << 4) + MVT_Channel , DAC_bits >> 8, DAC_bits & 0xff)))

def set_trigger_voltage_all_channels(voltage, ser_list, T20_num):
    '''
    Set the trigger voltage of all channel.
    
    This function includes the calibration of the MVT chanels
    and the offset Voltages of the Front end boards.
    
    This program assumes that the FPGA calibration was performed at 600mV.
    
    Parameters
    ----------
    voltage : float
        trigger voltage in [V] [0, 0.6].
    ser_list : list
        list of two Opend Serial port objects.
    T20_num : int
        Declaration if programm is run on T20_0 or T20_1.
        Needet to load the correct calibration Data.
        
    Returns
    -------
    None.

    '''
    if(voltage < 0 or voltage > 0.6):
        raise ValueError('Trigger voltage must be between 0V and 0.6V')
        
    Front_end_offset = np.loadtxt('./SIPM_DC/T20_%d_SiPM_DC.csv'%(T20_num))
    
    FPGA_Calib_offset_tuple = (np.loadtxt('./FPGA-Calib-Meas/FPGA_Calibration_MVT0_T20_%d0.txt'%(T20_num)).T[0],
                               np.loadtxt('./FPGA-Calib-Meas/FPGA_Calibration_MVT0_T20_%d1.txt'%(T20_num)).T[0])
    
    
    for i in range(len(ser_list)):
        ser = ser_list[i]
        for channel in range(8):
            combined_voltage = voltage + Front_end_offset[int(i*8+channel)] + 0.6 - FPGA_Calib_offset_tuple[i][channel]
            set_trigger_voltage_one_channel(combined_voltage, channel, 0, ser)

def set_bias_voltage_all_SiPMs(V_bias):
    '''
    Set the bias voltage of all SiPMs to V_bias.
    

    Parameters
    ----------
    V_bias : float
        Chosen trigger voltage [24.7, 30.1].

    Returns
    -------
    None.

    '''
    if(V_bias < 24.7):
        warnings.warn('V_bias < 24.7V, was set to 24.7V.')
    if(V_bias > 30.1):
        warnings.warn('V_bias > 30.1V, was set to 30.1V.')
        
    for backbone_no in range(3):
        for SiPM_no in (1,5):
            set_V_bias(backbone_no, SiPM_no, V_bias)

def sync_and_reset_clock():
    GPIO.output(5, True)
    GPIO.output(5, False)
    
def read_data(N_max, V_bias, V_trigger, fname = './timestamps.csv'):
    
    if len(serial.tools.list_ports.comports()) < 4:
        raise ConnectionError('Only %d Boards of 2 connected.'% int(len(serial.tools.list_ports.comports())/2))

    # Setup the clock flor the FPGAs 
    Si5338A_config.setup()
    
    # Setup GPIO
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(5, GPIO.OUT)
    
    # Set the biasvoltage of the SiPMs
    set_bias_voltage_all_SiPMs(V_bias)
    
    # Connect to both readout boards
    serial_numbers_to_01 = {'2516330028C3': 0, #T20_00
                            '251633006EF6': 1, #T20_01
                            '251633006F03': 0, #T20_10
                            '251633006EFF': 1} #T20_11
    
    serial_numbers_to_T20 = {'2516330028C3': 0, #T20_00
                             '251633006EF6': 0, #T20_01
                             '251633006F03': 1, #T20_10
                             '251633006EFF': 1} #T20_11
    
    adress_tupel = ['', '']
    adress_tupel[serial_numbers_to_01[serial.tools.list_ports.comports()[0].serial_number]] = serial.tools.list_ports.comports()[0].device
    adress_tupel[serial_numbers_to_01[serial.tools.list_ports.comports()[2].serial_number]] = serial.tools.list_ports.comports()[2].device
    
    T20_num = serial_numbers_to_T20[serial.tools.list_ports.comports()[0].serial_number]
    
    ser0 = serial.Serial(adress_tupel[0], 921600)#921600
    ser1 = serial.Serial(adress_tupel[1], 921600)#921600
    
    # Set the trigger voltage of all chnnels
    set_trigger_voltage_all_channels(V_trigger, (ser0, ser1), T20_num)
    
    # reset FIFOs
    reset_all_FIFOs(ser0)
    reset_DeSer_FIFOs(ser0)
    reset_all_FIFOs(ser1)
    reset_DeSer_FIFOs(ser1)
    
    
    # Begin decoding
    sequence = np.array([0, 1, 1, 1, 1, 0, 0], dtype = np.uint8)

    position0 = 0
    position1 = 0
    read0 = np.zeros(10, dtype = np.uint8)
    read1 = np.zeros(10, dtype = np.uint8)
    
    sync_and_reset_clock()
    
    # Decode the incoming data
    with tqdm(total = N_max) as pbar:
        n = 0
        with open(fname, 'w') as f:   
            f.write('# 800MHz counter, board (0 = Top, 1 = Bot), channel \n')

            ser0.read_all()
            ser1.read_all()
    
            while(n < N_max):
                if ser0.in_waiting != 0:
                    in0 = ser0.read(1)[0]
                    
                    if position0 < 7:
                        if (in0 >> 7) == sequence[position0]:
                            read0[position0] = np.uint8(in0)
                            position0 += 1
                        elif( (in0 >> 7) == 0):
                            read0[0] = np.uint8(in0)
                            position0 = 1
                        else:
                            position0 = 0 
                    elif(position0 == 9):
                        read0[position0] = np.uint8(in0)
                        payload = decode(read0, True)
                        if(payload[-1]): f.write('%20d, 0, %d \n'%(payload[0], payload[3]))
                        position0 = 0
                        pbar.update(1)
                        n += 1
                    else:
                        read0[position0] = np.uint8(in0)
                        position0 += 1
                        
                if ser1.in_waiting != 0:
                    in1 = ser1.read(1)[0]
                    
                    if position1 < 7:
                        if (in1 >> 7) == sequence[position1]:
                            read1[position1] = np.uint8(in1)
                            position1 += 1
                        elif( (in1 >> 7) == 0):
                            read1[0] = np.uint8(in1)
                            position1 = 1
                        else:
                            position1 = 0 
                    elif(position1 == 9):
                        read1[position1] = np.uint8(in1)
                        payload = decode(read1, True)
                        if(payload[-1]): f.write('%20d, 1, %d \n'%(payload[0], payload[3]))
                        position1 = 0
                        pbar.update(1)
                        n += 1
                    else:
                        read1[position1] = np.uint8(in1)
                        position1 += 1

if __name__ == '__main__':
    read_data(N_max = 100000, V_bias = 30, V_trigger = 0.1, fname = './timestamps.csv')
