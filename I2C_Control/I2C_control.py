from smbus2 import SMBus, i2c_msg
import numpy as np

def get_i2C_Adress(backbone_no):
    '''
    Generates the i2c adresses of the devices on the chosen backbone.

    Parameters
    ----------
    backbone_no : int
        The backbone number of which the aresses should be generated.
        [0;7]

    Returns
    -------
    start_ad : np.array
        Array of adresses. [DAC1, DAC0, EEPROM, TestPulse]

    '''
    start_ad = np.array([0x22, 0x21, 0x51, 0x4c])
    # Those are the values we will use to XOR the addresses with
    
    xor_val = np.array([ 0x04, 0x0c, 0x08, 0x04, 0x1C, 0x04, 0x08, 0x04])
    for i in range(backbone_no): 
        start_ad = start_ad^xor_val[i]
    #print(list(map(hex, start_ad)))
    return start_ad


def V_bias_from_DAC(DAC):
    '''
    Calculats the coresponding voltage to a DAC value.

    Parameters
    ----------
    DAC : int
        DAC value 0-1023.

    Returns
    -------
    corresponding voltage min 24.7V max 30.1V.

    '''
    if(DAC < 0): DAC = 0
    elif(DAC > 1023): DAC = 1023
    return (11.5 + 2.5*float(DAC)/1023)*4.3/2 

def DAV_from_V_bias(V_bias):
    '''
    Calculats the corresponding 10bit DAC value to the given biasvoltage.

    Parameters
    ----------
    V_bias : float
        Biasvoltage.

    Returns
    -------
    DAC : int
        10bit DAC value corresponding to the biasvoltage.

    '''
    DAC =  round((V_bias * 2.0 / 4.3 - 11.5) * 1023 /2.5)
    if (DAC > 1023): DAC = 1023;  
    elif (DAC < 0 ): DAC = 0;
    return DAC


def set_V_bias(backbone_no, channel_no, V_bias):
    '''
    Sets the voltage of the chosen channel to the desired value.

    Parameters
    ----------
    backbone_no : int
        number of the backbone.
    channel_no : int
        number on the channel on the chosen backbone, [0; 7].
    V_bias : float
        desire biasvoltage in Volts. [24.7; 30.1].

    Returns
    -------
    None.

    '''
   
    DACs_adr = get_i2C_Adress(backbone_no)
    
    # mask is used to set the channels of the back bone to the corresponding channels of the DAC.
    mask = (1, 0, 3, 2); 
     
    # Each backbone has two DAC each with four channels 
    # here the needed adress and channel number are determined.
    if (channel_no < 4):
        i2c_adr = DACs_adr[0]
        chan_adr = mask[channel_no]
    else:
        i2c_adr = DACs_adr[1]
        chan_adr = mask[channel_no - 4]
     
    # The two data bytes which will be written to the DAC.
    out_num = DAV_from_V_bias(V_bias)
     
    '''
     Building of the three bytes:
         byte0: cammand + channel adress 
                            0000 DAC A
                            0001 DAC B
                            0010 DAC C
                            0011 DAC D
                            1111 All
                         
         byte1+2: 10 voltage bits followd by 4 don't care bits
    
     Commands:
     0 0 0 0 Write to Input Register n
     0 0 0 1 Update (Power Up) DAC Register n
     0 0 1 0 Write to Input Register n, Update (Power Up) All
     0 0 1 1 Write to and Update (Power Up) DAC Register n
     0 1 0 0 Power Down n
     0 1 0 1 Power Down Chip (All DAC’s and Reference)
     0 1 1 0 Select Internal Reference (Power Up Reference)
     0 1 1 1 Select External Reference (Power Down Internal Reference)
     1 1 1 1 No Operation
    '''
    
    COMM = 0b0011
    output_buffer = np.zeros(3, dtype = np.uint8)
    output_buffer[0] = COMM << 4
    output_buffer[0] = output_buffer[0] | chan_adr
    output_buffer[1] = (out_num >> 2) & 0xFF
    output_buffer[2] = (out_num << 6) & 0xFF

    with SMBus(1) as bus:
        msg = i2c_msg.write(i2c_adr, output_buffer)
        bus.i2c_rdwr(msg)
        
if __name__ == '__main__':
    set_V_bias(0, 1, 30)
    set_V_bias(0, 5, 30)
    set_V_bias(1, 1, 30)
    set_V_bias(1, 5, 30)
    set_V_bias(2, 1, 30)
    set_V_bias(2, 5, 30)




