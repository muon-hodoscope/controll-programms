# Controll Programms

This git is a collection af all programms that are needed for the PC side operation of the T20 experiment.

SIPM-Calibration are the programms needet to controll the oscilloscope and the back bone.
This will be used to set bias voltages and measure p.e. heights.

FPGA-Calib-Meas are programms to read data from the FPGA and to set the trigger threcholds of the diferent channels.
The script to calibrate the MVT trigger voltages is also included.

### Read_Timestamps
Read_timestamps is the main programm to read timestamps from both FPGAs
Serial_test.py can be used to debug (it only reads from one FPGA)

### SiPM_calib_example
this programm can be used to measure the p.e. of an SiPM

### I2C_control
Can be used to set biasvoltages. Just run the programm I2C_control.py. At the bottom of the file you can change channels and voltages.

It is important that the Xor vals of the get_i2C_Adress() in the I2C_control.py are the same as in the real setp and in the same order. Both T20_0 and T20_1 have to be the same.

### FPGA calib meas
The 2. channel of the function generator is the resetpulse and the 1. channelis the signal.

###
