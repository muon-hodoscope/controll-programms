import time
import pyvisa
import numpy as np
from tqdm import tqdm
import serial
import warnings
import os


class Measurement():
    '''
    This class provides the funktions to controll the SIPM voltages and take
    measurements with the oscilloscope.
    '''
    
    def __enter__(self, Osci = 'USB0::6833::1230::DS1ZC253304635::0::INSTR'):
        '''
        Init the measurement by connecting to the osci.
        
        Parameters
        ----------
        Osci : String, optional
            Port of the oscilloscope. The default is 'USB0::6833::1230::DS1ZC253304635::0::INSTR'.

        Raises
        ------
        ConnectionError
            If connection to the oscilloscope fails.

        Returns
        -------
        None.

        '''
      
        self.rm = pyvisa.ResourceManager('@py')
        try:
            self.rigold = self.rm.open_resource(Osci)
            print('SETUP:   VISA connected')

        except:
            self.rigold = None
            raise ConnectionError('Visa init. Error')
        
        return self

    def set_up_osci(self, scale, offset, time_scale, channel = 1, coupling = 'AC'):
        '''
        Resets the oscilloscope and configures it to the chosen state.


        Parameters
        ----------
        scale : float
            The voltge scale of the chosen channel in [V].
        offset : float
            The voltage offset of the chosen channel in [V].
        time_scale : float
            The time scale in [s].
        channel : int, optional
            The channel which will be configured. The default is 1.
        coupling : str, optional
            Coupling of the Channel. Either 'AC' or 'DC'. The default is 'AC'.

        Returns
        -------
        None.

        '''
        if not( coupling in ('AC', 'DC')):
            raise ValueError('coupling must be "AC" or "DC"')
        
        if scale < 0 or time_scale < 0:
            raise ValueError('scale an time_scale must be positive.')
        
        
        self.rigold.write('*RST')
        time.sleep(5)
        self.rigold.write(':CHAN1:COUP ' + coupling)
        self.rigold.write(':CHAN1:PROB 1')
        self.rigold.write(':CHAN1:SCAL  %.5f'%scale)
        self.rigold.write(':CHAN1:OFFS %.5f'%offset)
        self.rigold.write(':TIM:MAIN:SCAL %.11f'%time_scale)
        time.sleep(3)


    def measure_frequency(self, V_trigger, N):
        rates = np.zeros(N)
        self.rigold.write(':MEASure:COUNter:SOURCE CHAN1')
        self.rigold.write(':TRIGger:EDGe:LEVel %.6f'%(V_trigger))
        time.sleep(0.55)
        for i in range(N):
            time.sleep(0.55)
            rates[i] = (float(self.rigold.query(':MEASure:COUNter:VALue?')[:-1]))
        rates = np.array(rates)
        if len(rates) == 1:
            std = -1
        else:
            std = rates.std(ddof = 1)/np.sqrt(len(rates))
        return np.array((rates.mean(), std))

    def measure_trigger_range(self, path, N=6, V_trigger_start = 0.5, V_trigger_stop = 20, V_trigger_step = 0.5):
        if(not os.path.isdir(path)): os.makedirs(path)
        
        trigger_voltages = np.arange(V_trigger_start, V_trigger_stop+V_trigger_step, V_trigger_step)/1000
        np.savetxt(path + '/trigger_voltages.csv', trigger_voltages)

        data = np.zeros((len(trigger_voltages),2))
        for i in tqdm(range(len(trigger_voltages)), 'Trigger Voltages'):
            data[i] = self.measure_frequency(trigger_voltages[i], N)
        
        np.savetxt(path + '/rates.csv', data.T)

        return data.T[0], data.T[1], trigger_voltages*1000
    
    
    def __exit__(self, type, value, tb):
        self.rm.close()

if __name__ == '__main__':
    with Measurement() as Meas:
        Meas.set_up_osci(5e-3, -12e-3, 20e-9)
        Meas.measure('./Licht_dicht_test/Zu', 10, 0.5, 0.5)
