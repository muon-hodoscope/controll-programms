import numpy as np
import scipy as sc
from praktikum import analyse
import matplotlib.pyplot as plt
import warnings


'''
This class provides function to determine the p.e. of an SIPM by analysing
the measured noise rates.
'''

def calc_deriv(x, y, yerr, n):
    '''
    Calculates the derivative of discrete data points.
    To supress noise a line is fitted to each datapoint and its direct
    neighbours.

    Parameters
    ----------
    x : array_like
        x coordinates of the data points.
    y : array_like, 
        y coordinates of the data points..
    n : int
        Range of neighbouring points to be included in the line fits.
        If x_i is the current point to whitch a line is fitted the points 
        [x_(i-n), x_(i+n)] are used for the fit.
        
        Higher n will result in lower noise but will flatten the resuling funktion.

    Returns
    -------
    diff : numpy.ndarray
        The derivative of the input points.

    '''
    x, y, yerr = np.array(x), np.array(y), np.array(yerr)
    
    diff = np.zeros_like(x)
    diff_err = np.zeros_like(x)

    for i in range(len(x)):
        # i+n+1 to account for the off by one of array slicing
        nmin, npos = i-n, i+n+1
        # account for edge points
        if i < n:
            nmin = 0
        if i + n > len(x):
            npos = len(x) - 1
        #select points to be used in fit
        x_temp = x[nmin: npos]
        y_temp = y[nmin: npos]
        yerr_temp = yerr[nmin: npos]
        # fit line to points. Points get weighted by their distance to the central point. 
        fit_err = (abs(x_temp-x[i])/abs(x[i-1]-x[i]) + 1)*yerr_temp
        fit = analyse.lineare_regression(x_temp, y_temp, fit_err)
        diff[i] = fit[0]
        diff_err[i] = fit[1]
    return diff, diff_err

def find_peaks(x, y, prominence = 0.2, plot = False):
    x, y = np.array(x, dtype = np.float64), np.array(y, dtype = np.float64)
    
    if (len(x.shape) != 1 or len(y.shape) != 1 ):
        raise ValueError('x and y must be on dimensional.')
    
    if (len(x) != len(y)):
        raise ValueError('x and y must have the same length.')

    peaks, properties = sc.signal.find_peaks(y, prominence = prominence)
    
    if(plot):
        fig, ax = plt.subplots()
        ax.scatter(x, y, c = 'tab:blue', label = 'data')
        ax.scatter(x[peaks], y[peaks], c = 'tab:red', sizes = np.ones_like(peaks)*8, marker = 'x', label = 'peaks')

    if(len(peaks) < 2):
       warnings.warn('Less than two peaks were found. Try changing the prominence.')

    return(peaks)

def fit_peaks(x, y, y_err, prominence = 0.2, N = 3, plot = False):
    x, y, y_err = np.array(x, dtype = np.float64), np.array(y, dtype = np.float64), np.array(y_err, dtype = np.float64)
    
    if (len(x.shape) != 1 or len(y.shape) != 1 ):
        raise ValueError('x and y must be on dimensional.')
    
    if (len(x) != len(y)):
        raise ValueError('x and y must have the same length.')
        
    if(N < 1):
        raise ValueError('N must be greater than 0.')
        
    peaks, properties = sc.signal.find_peaks(y, prominence = prominence)
            
    if(plot):
        fig, ax = plt.subplots()
        ax.errorbar(x, y, yerr = y_err, c = 'tab:blue', fmt = '.', label = 'data')
        ax.scatter(x[peaks], y[peaks], c = 'tab:red', sizes = np.ones_like(peaks)*16, marker = 'x', label = 'peaks')

    if(len(peaks) < 2):
       warnings.warn('Less than two peaks were found. Try changing the prominence.')

    norm = lambda x, mu, sig, c0, c1: c0/np.sqrt(2*np.pi*sig**2)*np.exp(-1/2*((x-mu)/sig)**2) + c1
    
    sig0 = abs(x[peaks[0]-N] - x[peaks[0] + N])
    p0 = (x[peaks[0]], sig0, y[peaks[0]]* np.sqrt(2*np.pi*sig0**2), 0)
    fit1 = sc.optimize.curve_fit(norm, x[peaks[0]-N:peaks[0]+N+1], y[peaks[0]-N:peaks[0]+N+1], sigma = y_err[peaks[0]-N:peaks[0]+N+1], p0 = p0 )
    
    sig0 = abs(x[peaks[1]-N] - x[peaks[1] + N])
    p0 = (x[peaks[1]], sig0, y[peaks[1]]* np.sqrt(2*np.pi*sig0**2), 0)
    fit2 = sc.optimize.curve_fit(norm, x[peaks[1]-N:peaks[1]+N+1], y[peaks[1]-N:peaks[1]+N+1], sigma = y_err[peaks[1]-N:peaks[1]+N+1], p0 = p0 )
    
    if(plot):
        x_temp = np.linspace(x[peaks[0]-N], x[peaks[0]+N], 100)
        ax.plot(x_temp, norm(x_temp, *fit1[0]), color = 'tab:green', label = 'fit1')
        x_temp = np.linspace(x[peaks[1]-N], x[peaks[1]+N], 100)
        ax.plot(x_temp, norm(x_temp, *fit2[0]), color = 'tab:green', label = 'fit2')
        ax.legend()
        ax.grid()
        fig.show()
        
    return(abs(fit1[0][0] - fit2[0][0]))