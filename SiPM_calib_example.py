import sys
sys.path.insert(0,'./SIPM-Calibration/')
sys.path.insert(0,'./I2C_Control/')

from I2C_control import set_V_bias
import Osci_Measurment
import Analyse
import numpy as np
import matplotlib.pyplot as plt

V_bias_start = 26

# Measurement is the class which manages the comunication with the oscilloscope.
# When it is initilazed it connects and at the end of the programm it needs to be closed.
with Osci_Measurment.Measurement() as Meas:
    #   scale, offset, time_scale, channel = 1, coupling = 'AC'
    Meas.set_up_osci(5e-3, -12e-3, 20e-9)
    
    # Set the biasvoltage of the second channel of the first backbone to 26V
    set_V_bias(0, 1, 29)
    # Measure the trigger rate at Trigger voltages between 0.5mV and 20 mV in 0.5mV stept
    # Each neasurment is repeated N = 6 time in order to calculate an statistical error on the measurment
    # The data is saved to './Measument0'
    rates, rates_err, V_trigger = Meas.measure_trigger_range('./Measument0', N=6, V_trigger_start = 0.5, V_trigger_stop = 20, V_trigger_step = 0.5)
    
    # Calculate the logarithm of the rates and mask falty values
    rateslog, rates_errlog = np.log10(rates), rates_err/(rates*np.log(10))
    a = np.ma.masked_invalid(rateslog).mask == False
    rateslog, rates_errlog = rateslog[a], rates_errlog[a]
    
    # Plot the measure rates
    fig_rate, ax_rate = plt.subplots()
    ax_rate.errorbar(V_trigger, rateslog, yerr = rates_errlog, fmt = '.')
    ax_rate.set_xlabel('$V_{trigger} [mV]$'), ax_rate.set_ylabel('$log_{10}(rate)$'), ax_rate.grid()
    
    # Calculate the derivative of the measured data.
    # To supress noise a line is fitted to each datapoint and its nearest neighbour.
    # For for a point i and n = 4 the points ratelog[i-4,i+5] are used for the fit.
    diff, diff_err = Analyse.calc_deriv(V_trigger, rateslog, rates_errlog, n = 4)
    
    # Find the peak of the finger spektrum retuns the indeces of the found peaks
    # Prominance ist the minimal apsolut height of the peaks
    peaks = Analyse.find_peaks(V_trigger, -diff, prominence = 0.05 )
    
    # Plot the derivative
    fig_deriv, ax_deriv = plt.subplots()
    ax_deriv.errorbar(V_trigger, -diff, yerr = diff_err, fmt = '.')
    ax_deriv.vlines(V_trigger[peaks], ax_deriv.set_ylim()[0], -diff[peaks], color = 'tab:red', label = 'peaks')
    ax_deriv.set_xlabel('$V_{trigger} [mV]$'), ax_deriv.set_ylabel('$\\frac{d}{dV}log_{10}(rate)$'), ax_deriv.legend(), ax_deriv.grid()
    
    print(abs(V_trigger[peaks[0]]-V_trigger[peaks[1]]))