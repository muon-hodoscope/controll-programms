#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 28 17:10:22 2023

@author: erik
"""
import serial.tools.list_ports
import serial
import time
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm
from numba import njit


PARITY1 = lambda i: i == 1
PARITY2 = lambda i: PARITY1(i & 0x01) ^ PARITY1(i >> 1)
PARITY4 = lambda i: PARITY2(i & 0x03) ^ PARITY2(i >> 2)
PARITY8 = lambda i: PARITY4(i & 0x0f) ^ PARITY4(i >> 4)

parity_look_up = np.array([PARITY8(np.uint8(i)) for i in range(256)], dtype = bool)

@njit
def encode(Timestamp, Channel_number, MVT_Channel = 0, Is_rising = True):
    write_buffer = np.zeros(10, dtype = np.uint8);
    write_buffer[0] =  (Is_rising <<  np.uint8(6)) |  (MVT_Channel <<  np.uint8(4)) |  (Channel_number <<  np.uint8(1) ) |  (Timestamp >>  np.uint8(63))
    write_buffer[1] =  np.uint8(1 << 7) |  ( (Timestamp >> np.uint8(56)) & np.uint8(0x7F))
    write_buffer[2] =  np.uint8(1 << 7) |  ( (Timestamp >> np.uint8(49)) & np.uint8(0x7F))
    write_buffer[3] =  np.uint8(1 << 7) |  ( (Timestamp >> np.uint8(42)) & np.uint8(0x7F))
    write_buffer[4] =  np.uint8(1 << 7) |  ( (Timestamp >> np.uint8(35)) & np.uint8(0x7F))
    write_buffer[5] =  ( (Timestamp >> np.uint8(28)) & np.uint8(0x7F))
    write_buffer[6] =  ( (Timestamp >> np.uint8(21)) & np.uint8(0x7F))
    write_buffer[7] =  ( (Timestamp >> np.uint8(13)) & np.uint8(0xFF))
    write_buffer[8] =  ( (Timestamp >> np.uint8( 5)) & np.uint8(0xFF))
    write_buffer[9] =  ( (Timestamp << np.uint8( 3)) & np.uint8(0xFF))
    
    p0 = (write_buffer[7] >> 7)  ^ (write_buffer[8] >> 7) ^ (write_buffer[9] >> 7)
    p1 = parity_look_up[write_buffer[0]] ^ parity_look_up[write_buffer[1]] ^ parity_look_up[write_buffer[2]] ^ parity_look_up[write_buffer[3]] ^ parity_look_up[write_buffer[4]]
    p2 = parity_look_up[write_buffer[5]] ^ parity_look_up[write_buffer[6]] ^ parity_look_up[write_buffer[7]] ^ parity_look_up[write_buffer[8]] ^ parity_look_up[write_buffer[9]]
    write_buffer[9] = write_buffer[9] | (p0 << 2) | (p1 << 1) | (p2)

    #print('{:08b}\n{:08b}\n{:08b}\n{:08b}\n{:08b}\n{:08b}\n{:08b}\n{:08b}\n{:08b}\n{:08b}'.format(*write_buffer))
    return(write_buffer)

@njit
def decode(data_in, check_parity = True):
    parity = True
    if (check_parity == True):
        p0 = ( data_in[7] >> 7)  ^ ( data_in[8] >> 7) ^ ( data_in[9] >> 7)
        p1 = parity_look_up[ data_in[0]] ^ parity_look_up[ data_in[1]] ^ parity_look_up[ data_in[2]] ^ parity_look_up[ data_in[3]] ^ parity_look_up[ data_in[4]]
        p2 = parity_look_up[ data_in[5]] ^ parity_look_up[ data_in[6]] ^ parity_look_up[ data_in[7]] ^ parity_look_up[ data_in[8]] ^ (parity_look_up[ data_in[9] & 0xf8])
        parity = (np.uint8(p0)*4+np.uint8(p1)*2+np.uint8(p2)) == (data_in[9] & 0x07)
    
    is_rising =  (data_in[0] & 0x40) == 0x40
    Mvt_channel = (data_in[0] & 0x30) >> 4
    Channel = (data_in[0] & 0x0e) >> 1
    timestamp  = (np.uint64(data_in[0] & 0x01) << np.uint8(63))
    timestamp += (np.uint64(data_in[1] & 0x7F) << np.uint8(56))
    timestamp += (np.uint64(data_in[2] & 0x7F) << np.uint8(49))
    timestamp += (np.uint64(data_in[3] & 0x7F) << np.uint8(42))
    timestamp += (np.uint64(data_in[4] & 0x7F) << np.uint8(35))
    timestamp += (np.uint64(data_in[5] & 0x7F) << np.uint8(28))
    timestamp += (np.uint64(data_in[6] & 0x7F) << np.uint8(21))
    timestamp += (np.uint64(data_in[7] & 0xFF) << np.uint8(13))
    timestamp += (np.uint64(data_in[8] & 0xFF) << np.uint8( 5))
    timestamp += (np.uint64(data_in[9] & 0xF8) >> np.uint8( 3))
    return(timestamp, is_rising, Mvt_channel, Channel, parity)    
    
def reset_all_fifos(ser):
    ser.write(bytes((0x01,)))

def reset_DeSer_fifos(ser):
    ser.write(bytes((0x02,)))


    
adress = serial.tools.list_ports.comports()[2].device
ser = serial.Serial(adress, 921600)#921600

reset_all_fifos(ser)
reset_DeSer_fifos(ser)

position = 0
sequence = np.array([0, 1, 1, 1, 1, 0, 0], dtype = np.uint8)
read = np.zeros(10, dtype = np.uint8)

t0 = time.time()

n = 10000
with tqdm(total=n) as pbar:
    with open('./timestamps.txt', 'w') as f:
        i = 0
        ser.read_all()
        while(i < n):
            a = ser.read(1)[0]
            #print('{:08b}'.format(a))    
            if position < 7:
                if (a >> 7) == sequence[position]:
                    read[position] = np.uint8(a)
                    position += 1
                elif( (a >> 7) == 0):
                    read[0] = np.uint8(a)
                    position = 1
                else:
                    position = 0 
            elif(position == 9):
                read[position] = np.uint8(a)
                payload = decode(read, True)
                if(payload[-1]): f.write('%d, %d, %d, \n'%(payload[0], payload[2], payload[3]))
                position = 0
                pbar.update(1)
                i += 1
            else:
                read[position] = np.uint8(a)
                position += 1

print(time.time()- t0)

data = np.loadtxt('./timestamps.txt', dtype= np.uint64, delimiter= ',', usecols = 0)
data = data[2:]
data = (data)/8e8
print(data.mean())


fig, ax = plt.subplots()
ax.scatter(range(len(data)), data)
ax.set_ylabel('time stamp [s]')
ax.set_xlabel('# of read out')


'''
print('mean []')

diff = data[1:]-data[:-1]
diff = diff[~np.greater(abs(diff), 20)]
fig, ax = plt.subplots()
ax.hist(diff, bins = 50)
ax.set_ylabel('$\\Dela t$')
ax.set_ylabel('#')

deltat = 0.1 #s 
intervalls = []
inter = 0
tempt = 0
for i in range(len(diff)):
    tempt = tempt + diff[i]
    if(tempt > deltat):
        intervalls.append(inter)
        inter = 0
        tempt = 0
    else:
        inter = inter + 1

intervalls = np.array(intervalls)
    
fig, ax = plt.subplots()
ax.hist(intervalls, bins = intervalls.max())
ax.set_ylabel('# in intervall')
ax.set_ylabel('#')
'''
'''
fig, ax = plt.subplots()
ax.scatter(range(len(data)-1), ((data[1:] - data[0:-1])*1000))
ax.set_ylim((0, ax.set_ylim()[1]*1.1))
ax.set_ylabel('rate [kHz]')
ax.set_xlabel('# of read out')
'''